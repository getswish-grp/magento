/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/view/form/element/email',
        'Magento_Checkout/js/model/quote',
        'mage/url',
        'Magento_Ui/js/modal/modal',
        'Magento_Checkout/js/checkout-data',
    ],
    function ($, Component, customerData, email, quote, url, modal, checkoutData) {
        'use strict';
        const paymentUrl = url.build('swish/request/payment'),
            paymentResultUrl = url.build('swish/request/paymentresult'),
            cancelPaymentUrl = url.build('swish/request/cancelpayment'),
            getMobileTokenUrl = url.build('swish/request/getmobiletoken'),
            getCheckOpenTransactionUrl = url.build('swish/checkout/checkopentransaction'),
            body = $('body').loader();

        return Component.extend({
            paymentReference: null,
            resultUrl: null,
            isRedirected: false,
            isRedirectInProgress: false,
            isCancelled: false,
            defaults: {
                template: 'Swish_Payment/payment/swish',
            },
            currentTelephone: quote.billingAddress()
                ? quote.billingAddress().telephone
                : quote.shippingAddress().telephone,
            transactionInProcess: false,
            requestCreationInProcess: false,
            grandTotal: quote.totals().grand_total + ' ' + quote.totals().base_currency_code,

            /**
             * Close action for modal's events
             *
             * @param popup
             */
            addCloseModalAction: function (popup) {
                let self = this;

                popup.on('modalclosed', function () {
                    self.cancelPayment();
                });

                $(window).bind('beforeunload', function (e) {
                    self.cancelPayment(true, e);
                });

                window.onhashchange = function () {
                    if (window.location.hash !== '#payment') {
                        popup.modal('closeModal');
                    }
                }
            },

            /**
             * Get Mailing address
             *
             * @returns {*}
             */
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },

            /**
             * Cancel payment method
             *
             * @param tabClosed
             */
            cancelPayment: function (tabClosed = false, event = false) {
                if (this.isCancelled || !this.transactionInProcess || this.paymentReference) {
                    return;
                }

                if (event) {
                    event.preventDefault();
                }

                let self = this;

                if (this.requestCreationInProcess) {
                    setTimeout(function () {
                        self.cancelRequest(tabClosed);
                    }, 1000)
                } else {
                    self.cancelRequest(tabClosed);
                }
            },

            /**
             * Cancel payment action
             *
             * @param tabClosed
             */
            cancelRequest: function (tabClosed) {
                body.loader('show');

                let self = this,
                    qrCodePopup = $('#swish-use-qr-code');

                self.setIsCancelled(true);

                $.ajax({
                    url: cancelPaymentUrl,
                    method: 'POST',
                    data: {
                        'url': this.resultUrl,
                        'isTabClosed': tabClosed
                    }
                }).done(function (response) {
                    if (response.status !== 'error') {
                        alert($.mage.__('Payment is cancelled'));
                    } else {
                        alert($.mage.__(response.message));
                        location.reload();
                    }

                    self.setIsCancelled(false);
                    self.setTransactionInProcess(false);
                    $('.swish-use-phone-popup-hint').hide();
                    $('.phone-input-conteiner').show();
                    $('.swish-start-payment').prop('disabled', false);
                    body.loader('hide');
                    if (qrCodePopup.modal()) {
                        qrCodePopup.modal('closeModal');
                    }
                });
            },

            /**
             * Check if billing address exist, if not - disable swish payment
             */
            checkAddressAndEmail: function () {
                let customer = customerData.get('customer');

                if (!quote.billingAddress() || (!customer().firstname && !email().validateEmail(false))) {
                    $('.swish-button').prop('disabled', true);
                }

                quote.billingAddress.subscribe(function () {
                    if (quote.billingAddress()) {
                        $('.swish-button').prop(
                            'disabled',
                            (!customer().firstname && !email().validateEmail(false)) || !quote.billingAddress()
                        );
                    }
                });

                $('#customer-email').on('change', function () {
                    $('.swish-button').prop(
                        'disabled',
                        (!customer().firstname && !email().validateEmail(false)) || !quote.billingAddress()
                    );
                });
            },

            /**
             * Make payment throw mobile number
             */
            enterPhone: function () {
                let self = this,
                    popup = $('#swish-use-phone-popup'),
                    options = {
                        type: 'popup',
                        responsive: true,
                        innerScroll: true,
                        title: 'Enter mobile phone',
                        buttons: [
                            {
                                text: $.mage.__('Cancel'),
                                class: 'action secondary checkout swish-cancel-payment',
                                click: function () {
                                    $('.swish-start-payment').prop('disabled', false);
                                    this.closeModal();
                                }
                            },
                            {
                                text: $.mage.__('Pay'),
                                class: 'action primary checkout swish-start-payment',
                                click: function () {
                                    $('.action-update').click();
                                    self.pay();
                                    $('.swish-start-payment').prop('disabled', true);
                                }
                            }
                        ]
                };

                modal(options, popup);
                popup.modal('openModal');

                self.addCloseModalAction(popup);
            },

            /**
             * Get payment method data
             */
            getData: function () {
                return {
                    'method': this.item.method,
                    'po_number': null,
                    'additional_data': {
                        'payment_reference': this.paymentReference
                    }
                };
            },

            /**
             * QR code request method
             */
            getQrCode: function () {
                let self = this;

                body.loader('show');

                $.ajax({
                    url: paymentUrl,
                    method: 'GET',
                    data: {
                        isQRRequest: true,
                        email: checkoutData.getInputFieldEmailValue()
                    }
                }).done(function (response) {
                    if (response.status === 'error') {
                        alert($.mage.__('Sorry, something went wrong. Error message: ') + response.message);
                    } else {
                        body.loader('hide');

                        self.setResultUrl(response.url);
                        $('#swish-qr-code-container').html(response.svg);

                        self.getPaymentResult(response.url);
                    }
                });
            },

            /**
             * Retrieve payment result
             *
             * @param url
             */
            getPaymentResult: function (url) {
                let self = this;

                self.setTransactionInProcess(true);

                if (this.isCancelled) {
                    /**
                     * If tab/browser wasn't closed, but was closed popup,
                     * it will give a chance to user to try to pay one more time with new request
                     */
                    self.setIsCancelled(false);

                    return;
                }

                $.ajax({
                    url: paymentResultUrl,
                    method: 'GET',
                    data: {
                        'url': url,
                    }
                }).done(function (response) {
                    self.processPaymentResults(response);
                });
            },

            /**
             * Install Swish App to mobile device
             */
            installApp: function () {
                this.cancelPayment(false);
                if (navigator.userAgent.toLowerCase().indexOf('android') > -1) {
                    window.location =
                        'https://play.google.com/store/apps/details?id=se.bankgirot.swish&hl=en';
                }

                if (navigator.userAgent.toLowerCase().indexOf('iphone') > -1) {
                    window.location =
                        'itms-apps://itunes.apple.com/app/swish-betalningar/id563204724?mt=8';
                }
            },

            /**
             * Check does payee have open transaction
             */
            isPaymentAvailable: function () {
                let self = this,
                    params = new URLSearchParams(window.location.search);

                $.ajax({
                    url: getCheckOpenTransactionUrl,
                    method: 'POST'
                }).done(function (response) {
                    if (response) {
                        $('.payment-method-blocked').removeClass('_hidden');
                    } else {
                        $('.payment-method-unblocked').removeClass('_hidden');
                    }
                });

                self.checkAddressAndEmail();
                self.isMobile();
            },

            /**
             * Check is your device mobile
             *
             * @returns {boolean}
             */
            isMobile: function () {
                let self = this;

                if (navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i)) {
                    $('.swish-mobile').removeClass('_hidden');

                    window.addEventListener('hashchange', function () {
                        if (window.location.hash !== '#payment') {
                            self.cancelPayment(true);
                        }
                    }, false);

                    $(window).onbeforeunload = function (e) {
                        if (!this.paymentReference) {
                            e.preventDefault();
                            e.returnValue = '';
                        }
                    };

                    return true;
                } else {
                    $('.swish-desktop').removeClass('_hidden');

                    return false;
                }
            },

            /**
             * Create payment request
             */
            pay: function () {
                let self = this;

                body.loader('show');

                self.setTransactionInProcess(true);
                self.setRequestCreationInProcess(true);

                $.ajax({
                    url: paymentUrl,
                    method: 'POST',
                    data: {
                        phone: $('#telephone-value').val(),
                        email: checkoutData.getInputFieldEmailValue()
                    }
                }).done(function (response) {
                    body.loader('hide');
                    self.processPaymentResults(response);
                    self.setRequestCreationInProcess(false);
                });
            },

            /**
             * Process mobile payment
             */
            processMobile: function () {
                let self = this;

                if (self.isMobile()) {
                    $.ajax({
                        url: getMobileTokenUrl,
                        method: 'POST',
                        data: {
                            email: checkoutData.getInputFieldEmailValue()
                        }
                    }).done(function (response) {
                        if (response.status === 'error') {
                            alert($.mage.__('Sorry, something went wrong. Error message: ') + response.message);
                        } else {
                            self.setResultUrl(response.resultUrl);

                            window.onblur = function () {
                                self.setRedirectInProgress(false);
                                self.setRedirectedToTheApp(true);
                            };

                            self.runApp(response);

                            setTimeout(function () {
                                self.setRedirectInProgress(false);

                                if (!self.isRedirected) {
                                    self.installApp();
                                }

                                let deviceInfo = navigator.userAgent.toLowerCase();

                                if (deviceInfo.indexOf('iphone os 12') > -1
                                    || deviceInfo.indexOf('iphone os 13') > -1
                                ) {
                                    $('.swish-install-app-block').removeClass('_hidden');
                                }
                            }, 3000);
                        }
                    });
                } else {
                    alert($.mage.__('Sorry, your device is not mobile!'));
                }
            },

            /**
             * Process payment result
             *
             * @param response
             */
            processPaymentResults: function (response) {
                let self = this,
                    hintContainer = $('.swish-use-phone-popup-hint'),
                    phoneContainer = $('.phone-input-conteiner'),
                    payButton = $('.swish-start-payment'),
                    qrCodeModal = $('#swish-use-qr-code');

                switch (response.status) {
                    case 'error':
                        alert($.mage.__('Sorry, something went wrong. Error message: ') + response.message);
                        payButton.prop('disabled', false);
                        hintContainer.hide();
                        phoneContainer.show();
                        self.setTransactionInProcess(false);
                        qrCodeModal.modal('closeModal');

                        break;
                    case 'processing':
                        hintContainer.show();
                        phoneContainer.hide();
                        self.setResultUrl(response.url);
                        setTimeout(function () {
                            if (self.transactionInProcess) {
                                self.getPaymentResult(response.url);
                            }
                        }, response.delay_time);

                        break;
                    case 'paid':
                        self.setPaymentReference(response.paymentReference);
                        self.placeOrder();

                        break;
                    case 'cancelled':
                        hintContainer.hide();
                        phoneContainer.show();
                        alert($.mage.__('Payment is cancelled'));
                        self.setTransactionInProcess(false);
                        payButton.prop('disabled', false);
                        qrCodeModal.modal('closeModal');

                        break;
                }
            },

            /**
             * Open Swish App on your device
             *
             * @param response
             */
            runApp: function (response) {
                this.setRedirectInProgress(true);

                window.location.href = 'swish://paymentrequest?token='
                    + response.token
                    + '&callbackurl='
                    + response.callbackUrl;
            },

            /**
             * Mark payment as cancelled
             *
             * @param status
             */
            setIsCancelled: function (status) {
                this.isCancelled = status;
            },

            /**
             * Add payment reference to order
             *
             * @param paymentReference
             */
            setPaymentReference: function (paymentReference) {
                this.paymentReference = paymentReference;
            },

            /**
             * Mark request as redirected (Mobile app is opened)
             */
            setRedirectInProgress: function (state) {
                this.isRedirectInProgress = state;
            },

            /**
             * Mark request as redirected (Mobile app is opened)
             */
            setRedirectedToTheApp: function (state) {
                this.isRedirected = state;
            },

            /**
             * Mark request as created but not received
             *
             * @param state
             */
            setRequestCreationInProcess: function (state) {
                this.requestCreationInProcess = state;
            },

            /**
             * Set result url
             *
             * @param url
             */
            setResultUrl: function (url) {
                this.resultUrl = url;
            },

            /**
             * Set transaction in process
             *
             * @param state
             */
            setTransactionInProcess: function (state) {
                this.transactionInProcess = state;
            },

            /**
             * QR code result method
             */
            scanQRCode: function () {
                this.getQrCode();

                let self = this,
                    popup = $('#swish-use-qr-code'),
                    options = {
                        type: 'popup',
                        responsive: true,
                        innerScroll: true,
                        title: 'Scan QR Code',
                        buttons: [{
                            text: $.mage.__('Back'),
                            class: '',
                            click: function () {
                                this.closeModal();
                            }
                        }]
                    };

                $('.action-update').click();
                modal(options, popup);
                popup.modal('openModal');

                self.addCloseModalAction(popup);
            }
        });
    }
);
