<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Swish_Payment',
    __DIR__
);
