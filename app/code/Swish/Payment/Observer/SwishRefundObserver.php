<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Observer;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Framework\Event\Observer;
use Magento\Quote\Api\CartRepositoryInterface;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger as SwishLogger;
use Swish\Payment\Model\Payment\Swish;
use Swish\Payment\Model\SwishApi;
use Magento\Framework\Phrase;

class SwishRefundObserver extends AbstractDataAssignObserver
{
    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var SwishLogger
     */
    public $swishLogger;

    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var CartRepositoryInterface
     */
    public $quoteRepository;

    /**
     * SwishRefundObserver constructor
     *
     * @param Context $context
     * @param SwishApi $swishApi
     * @param SwishLogger $swishLogger
     * @param SwishHelper $swishHelper
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        Context $context,
        SwishApi $swishApi,
        SwishLogger $swishLogger,
        SwishHelper $swishHelper,
        CartRepositoryInterface $quoteRepository
    ) {
        $this->swishApi = $swishApi;
        $this->swishLogger = $swishLogger;
        $this->messageManager = $context->getMessageManager();
        $this->swishHelper = $swishHelper;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Refund order request
     *
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer) : void
    {
        $order = $observer->getCreditmemo()->getOrder();

        if ($order->getPayment()->getMethodInstance()->getCode() !== Swish::CODE) {
            return;
        }

        $quote = $this->quoteRepository->get($order->getQuoteId());

        try {
            $response = $this->swishApi->refund($order);

            do {
                sleep((int) $this->swishHelper->getConfig(SwishHelper::REQUEST_DELAY));

                $paymentReference = $this->swishApi->retrieveResult($response['Location']);
            } while (!$paymentReference);

            $this->swishLogger->addInfo(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Refunded',
                    false,
                    $paymentReference,
                    $order->getId()
                )
            );
        } catch (\RuntimeException $exception) {
            $this->swishLogger->addError(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Refund failed',
                    $exception->getMessage(),
                    '-',
                    $order->getId()
                )
            );

            throw new LocalizedException(new Phrase($exception->getMessage()));
        }
    }
}
