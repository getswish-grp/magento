<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Observer;

use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Framework\Event\Observer;
use Swish\Payment\Helper\SwishHelper;

class OrderMarkPaid extends AbstractDataAssignObserver
{
    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * SwishRefundObserver constructor
     *
     * @param SwishHelper $swishHelper
     */
    public function __construct(
        SwishHelper $swishHelper
    ) {
        $this->swishHelper = $swishHelper;
    }

    /**
     * Mark order as paid
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer) : void
    {
        $this->swishHelper->markSwishPaymentAsCompleted($observer->getOrder()->getQuoteId());
    }
}
