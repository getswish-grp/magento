<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Observer;

use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Sales\Model\Service\InvoiceService;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Model\Payment\Swish;

class InvoiceOrder implements ObserverInterface
{
    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var InvoiceService
     */
    public $invoiceService;

    /**
     * @var TransactionFactory
     */
    public $transactionFactory;

    /**
     * @var InvoiceSender
     */
    public $invoiceSender;

    /**
     * @var OrderInterface
     */
    private $order;

    /**
     * @var OrderResource
     */
    private $orderResource;

    /**
     * InvoiceOrder constructor
     *
     * @param InvoiceService $invoiceService
     * @param TransactionFactory $transactionFactory
     * @param SwishHelper $swishHelper
     * @param InvoiceSender $invoiceSender
     * @param OrderInterface $order
     * @param OrderResource $orderResource
     */
    public function __construct(
        InvoiceService $invoiceService,
        TransactionFactory $transactionFactory,
        SwishHelper $swishHelper,
        InvoiceSender $invoiceSender,
        OrderInterface $order,
        OrderResource $orderResource
    ) {
        $this->swishHelper = $swishHelper;
        $this->invoiceService = $invoiceService;
        $this->transactionFactory = $transactionFactory;
        $this->invoiceSender = $invoiceSender;
        $this->order = $order;
        $this->orderResource = $orderResource;
    }

    /**
     * Create/send invoice right after order is placed
     *
     * @param Observer $observer
     * @return void|null
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $orders = $observer->getOrders();

        if (is_array($orders)) {
            /** @var Order $order */
            foreach ($orders as $order) {
                $this->adjustOrder($order);
            }
        } else {
            $order = $observer->getOrder();
            if (is_object($order)) {
                $this->adjustOrder($order);
            }
        }
    }

    /**
     * @param Order $order
     * @throws NoSuchEntityException
     * @throws AlreadyExistsException
     */
    private function adjustOrder(Order $order): void
    {
        if (!$order->canInvoice()
            || $order->getState() !== Order::STATE_PROCESSING
            || !$this->swishHelper->getConfig(SwishHelper::SHOULD_CREATE_INVOICE)
            || $order->getPayment()->getMethodInstance()->getCode() !== Swish::CODE
        ) {
            return;
        }

        try {
            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->setRequestedCaptureCase(Invoice::CAPTURE_ONLINE);
            $invoice->register();

            $transaction = $this->transactionFactory->create()
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

            $transaction->save();

            if ($this->swishHelper->getConfig(SwishHelper::SHOULD_SEND_INVOICE)) {
                $this->invoiceSender->send($invoice);

                $order->addStatusHistoryComment(
                    __('Notified customer about invoice #%1.', $invoice->getId())
                )
                    ->setIsCustomerNotified(true);
                $this->orderResource->save($order);
            }
        } catch (\Exception $e) {
            $order->addStatusHistoryComment('Exception message: ' . $e->getMessage(), false);
            $this->orderResource->save($order);
        }
    }
}
