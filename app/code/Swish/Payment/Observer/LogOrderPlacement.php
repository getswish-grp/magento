<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Observer;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Framework\Event\Observer;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Model\Order;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger;
use Swish\Payment\Model\Payment\Swish;

class LogOrderPlacement extends AbstractDataAssignObserver
{
    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var Logger
     */
    public $swishLogger;

    /**
     * @var CartRepositoryInterface
     */
    public $quoteRepository;

    /**
     * SwishRefundObserver constructor
     *
     * @param SwishHelper $swishHelper
     * @param Logger $swishLogger
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        SwishHelper $swishHelper,
        Logger $swishLogger,
        CartRepositoryInterface $quoteRepository
    ) {
        $this->swishHelper = $swishHelper;
        $this->swishLogger = $swishLogger;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Log order placement
     *
     * @param Observer $observer
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(Observer $observer) : void
    {
        $orders = $observer->getOrders();
        if (is_array($orders)) {
            foreach ($orders as $order) {
                $this->swishLoggerInfo($order);
            }
        } else {
            $order = $observer->getOrder();
            if (is_object($order)) {
                $this->swishLoggerInfo($order);
            }
        }
    }

    /**
     * @param Order $order
     * @throws NoSuchEntityException
     */
    private function swishLoggerInfo(Order $order): void
    {
        if ($order->getPayment()->getMethodInstance()->getCode() !== Swish::CODE) {
            return;
        }

        $additionalInfo = $order->getPayment()->getAdditionalInformation();
        if (!isset($additionalInfo['payment_reference'])) {
            return;
        }

        $paymentReference = $additionalInfo['payment_reference'];
        $this->swishHelper->savePaymentReference($paymentReference, $order->getIncrementId());

        $this->swishLogger->addInfo(
            $this->swishHelper->getDataForLogger(
                $this->quoteRepository->get($order->getQuoteId()),
                'Payment success',
                false,
                $paymentReference,
                $order->getId()
            )
        );
    }
}
