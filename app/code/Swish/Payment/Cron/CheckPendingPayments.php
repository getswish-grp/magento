<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Cron;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\AddressFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteManagement;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger;
use Swish\Payment\Model\PaymentRecord;
use Swish\Payment\Model\ResourceModel\Pending\Collection;
use Swish\Payment\Model\SwishApi;

class CheckPendingPayments
{
    /**
     * @var Collection
     */
    public $pendingPaymentCollection;

    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var Logger
     */
    public $swishLogger;

    /**
     * @var QuoteManagement
     */
    public $quoteManagement;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var CartRepositoryInterface
     */
    public $quoteRepository;

    /**
     * @var AddressFactory
     */
    public $addressFactory;

    /**
     * CheckPendingPayments constructor
     *
     * @param Collection $pendingPaymentCollection
     * @param SwishApi $swishApi
     * @param Logger $swishLogger
     * @param SwishHelper $swishHelper
     * @param QuoteManagement $quoteManagement
     * @param CartRepositoryInterface $quoteRepository
     * @param AddressFactory $addressFactory
     */
    public function __construct(
        Collection $pendingPaymentCollection,
        SwishApi $swishApi,
        Logger $swishLogger,
        SwishHelper $swishHelper,
        QuoteManagement $quoteManagement,
        CartRepositoryInterface $quoteRepository,
        AddressFactory $addressFactory
    ) {
        $this->pendingPaymentCollection = $pendingPaymentCollection;
        $this->swishApi = $swishApi;
        $this->swishLogger = $swishLogger;
        $this->quoteManagement = $quoteManagement;
        $this->swishHelper = $swishHelper;
        $this->quoteRepository = $quoteRepository;
        $this->addressFactory = $addressFactory;
    }

    /**
     * Execute cron to create missed orders
     *
     * @throws NoSuchEntityException
     */
    public function execute() : void
    {
        $paymentList = $this->pendingPaymentCollection->addFieldToFilter('is_completed', false)->getItems();

        /** @var PaymentRecord $payment */
        foreach ($paymentList as $payment) {
            $quoteId = $payment->getQuoteId();
            $quote = $this->quoteRepository->get($quoteId);
            $quote->setIsActive(true);

            try {
                $customer = $quote->getCustomer();

                if (!$customer->getId()) {
                    $billingAddress = $quote->getBillingAddress();

                    $quote->setCustomerFirstname($billingAddress->getFirstname());
                    $quote->setCustomerLastname($billingAddress->getLastname());
                    $quote->setCustomerEmail($payment->getEmail());
                    $quote->setCustomerIsGuest(true);
                } elseif ($quote->getIsVirtual()) {
                    $customerAddress = $this->addressFactory->create()->load($customer->getDefaultBilling());
                    $quote->getBillingAddress()
                        ->setFirstname($customerAddress->getFirstname())
                        ->setLastname($customerAddress->getLastname())
                        ->setStreet($customerAddress->getStreet())
                        ->setCity($customerAddress->getCity())
                        ->setTelephone($customerAddress->getTelephone())
                        ->setPostcode($customerAddress->getPostcode())
                        ->setRegionId($customerAddress->getRegionId())
                        ->setCountryId($customerAddress->getCountryId());
                }

                if ($paymentReference = $this->swishApi->retrieveResult($payment->getLocation())) {
                    if (!in_array(
                        $paymentReference,
                        [SwishHelper::DECLINED_RESULT, SwishHelper::CANCELLED_RESULT],
                        true
                    )) {
                        $quote->setPaymentMethod('swish')->save();
                        $quote->getPayment()
                            ->importData([
                                'method' => 'swish',
                                'additional_data' => [
                                    'payment_reference' => $paymentReference
                                ]
                            ]);
                        $quote->save();

                        $this->quoteManagement->placeOrder($quoteId);
                    } else {
                        $this->swishHelper->getDataForLogger(
                            $quote,
                            'Payment is cancelled'
                        );
                    }

                    $this->swishHelper->markSwishPaymentAsCompleted($quoteId);
                }
            } catch (\RuntimeException $exception) {
                $this->swishLogger->addError(
                    $this->swishHelper->getDataForLogger(
                        $quote,
                        'Payment failed',
                        $exception->getMessage()
                    )
                );
            }
        }
    }
}
