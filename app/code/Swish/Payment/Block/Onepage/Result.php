<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */

namespace Swish\Payment\Block\Onepage;

use Magento\Customer\Model\AddressFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteManagement;
use RuntimeException;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Model\PaymentReferenceFactory;
use Swish\Payment\Model\SwishApi;

class Result extends Template
{
    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var PaymentReferenceFactory
     */
    public $paymentReferenceFactory;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var QuoteManagement
     */
    public $quoteManagement;
    /**
     * @var CartRepositoryInterface
     *
     */
    public $quoteRepository;

    /**
     * @var AddressFactory
     */
    public $addressFactory;

    /**
     * Result constructor.
     * @param Template\Context $context
     * @param SwishApi $swishApi
     * @param PaymentReferenceFactory $paymentReferenceFactory
     * @param SwishHelper $swishHelper
     * @param QuoteManagement $quoteManagement
     * @param CartRepositoryInterface $quoteRepository
     * @param AddressFactory $addressFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        SwishApi $swishApi,
        PaymentReferenceFactory $paymentReferenceFactory,
        SwishHelper $swishHelper,
        QuoteManagement $quoteManagement,
        CartRepositoryInterface $quoteRepository,
        AddressFactory $addressFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->swishApi = $swishApi;
        $this->paymentReferenceFactory = $paymentReferenceFactory;
        $this->swishHelper = $swishHelper;
        $this->quoteManagement = $quoteManagement;
        $this->quoteRepository = $quoteRepository;
        $this->addressFactory = $addressFactory;
    }

    /**
     * @param int $orderId
     * @return string
     */
    public function getViewOrderUrl($orderId)
    {
        return $this->getUrl('sales/order/view/', ['order_id' => $orderId, '_secure' => true]);
    }

    /**
     * @return string
     * @since 100.2.0
     */
    public function getContinueUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * Get Order id by payment reference
     *
     * @param $reference
     * @return mixed
     */
    public function getIncrementIdByPaymentReference($reference)
    {
        $paymentReference = $this->paymentReferenceFactory->create();
        $paymentReference->load($reference, 'payment_reference');

        return $paymentReference->getData('incrementId');
    }

    /**
     * @return array|bool[]|mixed
     * @throws CouldNotSaveException
     * @throws FileSystemException
     * @throws NoSuchEntityException
     */
    public function getResult()
    {
        try {
            do {
                sleep(1);

                $response = $this->swishApi->retrieveResult($this->getRequest()->getParam('resultUrl'));
            } while (!$response);

            if ($response === SwishHelper::DECLINED_RESULT || $response === SwishHelper::CANCELLED_RESULT) {
                return ['cancelled' => true];
            }

            return ['incrementId' => $this->placeOrder($response)];
        } catch (RuntimeException $exception) {
            return ['error' => $exception->getMessage()];
        }
    }

    /**
     * @param $paymentReference
     * @return mixed
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function placeOrder($paymentReference)
    {
        $quoteId = $this->getRequest()->getParam('quoteId');
        $quote = $this->quoteRepository->get($quoteId);

        $customer = $quote->getCustomer();

        if (!$customer->getId()) {
            $billingAddress = $quote->getBillingAddress();

            $quote->setCustomerFirstname($billingAddress->getFirstname());
            $quote->setCustomerLastname($billingAddress->getLastname());
            $quote->setCustomerEmail($this->getRequest()->getParam('email'));
            $quote->setCustomerIsGuest(true);
        } elseif ($quote->getIsVirtual()) {
            $customerAddress = $this->addressFactory->create()->load($customer->getDefaultBilling());
            $quote->getBillingAddress()
                ->setFirstname($customerAddress->getFirstname())
                ->setLastname($customerAddress->getLastname())
                ->setStreet($customerAddress->getStreet())
                ->setCity($customerAddress->getCity())
                ->setTelephone($customerAddress->getTelephone())
                ->setPostcode($customerAddress->getPostcode())
                ->setRegionId($customerAddress->getRegionId())
                ->setCountryId($customerAddress->getCountryId());
        }

        $quote->setPaymentMethod('swish')->save();
        $quote->getPayment()
            ->importData([
                'method' => 'swish',
                'additional_data' => [
                    'payment_reference' => $paymentReference
                ]
            ]);
        $quote->save();

        if ($quote->getIsActive()) {
            $this->quoteManagement->placeOrder($quoteId);
        }

        return $this->getIncrementIdByPaymentReference($paymentReference);
    }
}
