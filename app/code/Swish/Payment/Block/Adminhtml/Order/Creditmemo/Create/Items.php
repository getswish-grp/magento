<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */

namespace Swish\Payment\Block\Adminhtml\Order\Creditmemo\Create;

use Magento\Backend\Block\Widget\Button;
use Magento\Sales\Block\Adminhtml\Order\Creditmemo\Create\Items as SourceItems;
use Swish\Payment\Model\Payment\Swish;

class Items extends SourceItems
{
    /**
     * Prepare child blocks
     *
     * @return SourceItems
     */
    protected function _prepareLayout()
    {
        $swishLabel = $this->getOrder()->getPayment()->getMethod() === Swish::CODE
            ? 'Refund Through Swish'
            : 'Refund Offline';
        $onclick = "submitAndReloadArea($('creditmemo_item_container'),'" . $this->getUpdateUrl() . "')";
        $this->addChild(
            'update_button',
            Button::class,
            ['label' => __('Update Qty\'s'), 'class' => 'update-button', 'onclick' => $onclick]
        );

        if ($this->getCreditmemo()->canRefund()) {
            if ($this->getCreditmemo()->getInvoice() && $this->getCreditmemo()->getInvoice()->getTransactionId()) {
                $this->addChild(
                    'submit_button',
                    Button::class,
                    [
                        'label' => __('Refund'),
                        'class' => 'save submit-button refund primary',
                        'onclick' => 'disableElements(\'submit-button\');submitCreditMemo()'
                    ]
                );
            }
            $this->addChild(
                'submit_offline',
                Button::class,
                [
                    'label' => __($swishLabel),
                    'class' => 'save submit-button primary',
                    'onclick' => 'disableElements(\'submit-button\');submitCreditMemoOffline()'
                ]
            );
        } else {
            $this->addChild(
                'submit_button',
                Button::class,
                [
                    'label' => __($swishLabel),
                    'class' => 'save submit-button primary',
                    'onclick' => 'disableElements(\'submit-button\');submitCreditMemoOffline()'
                ]
            );
        }

        return $this;
    }
}
