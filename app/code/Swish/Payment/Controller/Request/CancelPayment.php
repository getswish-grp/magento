<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Controller\Request;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Framework as Framework;
use Magento\Quote\Api\CartRepositoryInterface;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger as SwishLogger;
use Swish\Payment\Model\SwishApi;

class CancelPayment extends Framework\App\Action\Action
{
    /**
     * @var SwishLogger
     */
    public $swishLogger;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var Framework\Controller\ResultFactory
     */
    public $resultFactory;

    /**
     * @var CartRepositoryInterface
     */
    public $quoteRepository;

    /**
     * @var Cart
     */
    public $cart;

    /**
     * Payment constructor
     *
     * @param Framework\App\Action\Context $context
     * @param SwishApi $swishApi
     * @param SwishHelper $swishHelper
     * @param SwishLogger $swishLogger
     * @param Cart $cart
     * @param Framework\Controller\ResultFactory $resultFactory
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        Framework\App\Action\Context $context,
        SwishApi $swishApi,
        SwishHelper $swishHelper,
        SwishLogger $swishLogger,
        Cart $cart,
        Framework\Controller\ResultFactory $resultFactory,
        CartRepositoryInterface $quoteRepository
    ) {
        parent::__construct($context);
        $this->swishLogger = $swishLogger;
        $this->swishHelper = $swishHelper;
        $this->swishApi = $swishApi;
        $this->resultFactory = $resultFactory;
        $this->quoteRepository = $quoteRepository;
        $this->cart = $cart;
    }

    /**
     * Execute cancel payemnt request
     *
     * @return Framework\App\ResponseInterface|Framework\Controller\ResultInterface
     * @throws Framework\Exception\NoSuchEntityException
     * @throws Exception
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);
        $url = $this->getRequest()->getParam('url');
        $isTabClosed = $this->getRequest()->getParam('isTabClosed');
        $quoteId = $this->cart->getQuote()->getId();

        try {
            $status = $this->swishApi->cancelPayment($url);
        } catch (\RuntimeException $exception) {
            $quote = $this->quoteRepository->get($quoteId);

            $this->swishLogger->addError(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Payment Cancellation Failed',
                    $exception->getMessage()
                )
            );

            // If tab is closed and we got the error that we can't cancel order,
            // this is situation when customer already paid for order, but order was not placed.
            // Because that we disable this quote to make paid quote changing impossible
            if ($isTabClosed) {
                $quote->setIsActive(false);
                $quote->save();
            }

            return $resultJson->setData([
                'status' => 'error',
                'message' => $exception->getMessage()
                    . ' Please check your account for more details.'
                    . ' If the money is withdrawn from your account the order will be created.'
            ]);
        }

        $this->swishHelper->deletePaymentFromPendingList($quoteId);

        return $resultJson->setData(['status' => $status]);
    }
}
