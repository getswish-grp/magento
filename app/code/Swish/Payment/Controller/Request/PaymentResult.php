<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Controller\Request;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Framework as Framework;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger as SwishLogger;
use Swish\Payment\Model\SwishApi;

class PaymentResult extends Framework\App\Action\Action
{
    /**
     * @var SwishLogger
     */
    public $swishLogger;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var Framework\Controller\ResultFactory
     */
    public $resultFactory;

    /**
     * @var Cart
     */
    public $cart;

    /**
     * Payment constructor.
     * @param Framework\App\Action\Context $context
     * @param SwishApi $swishApi
     * @param SwishHelper $swishHelper
     * @param SwishLogger $swishLogger
     * @param Framework\Controller\ResultFactory $resultFactory
     * @param Cart $cart
     */
    public function __construct(
        Framework\App\Action\Context $context,
        SwishApi $swishApi,
        SwishHelper $swishHelper,
        SwishLogger $swishLogger,
        Framework\Controller\ResultFactory $resultFactory,
        Cart $cart
    ) {
        parent::__construct($context);
        $this->swishLogger = $swishLogger;
        $this->swishHelper = $swishHelper;
        $this->swishApi = $swishApi;
        $this->resultFactory = $resultFactory;
        $this->cart = $cart;
    }

    /**
     * Execute payment retrieve result request
     *
     * @return Framework\App\ResponseInterface|Framework\Controller\ResultInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);
        $url = $this->getRequest()->getParam('url');
        $quote = $this->cart->getQuote();

        try {
            $response = $this->swishApi->retrieveResult($url);

            if ($response === SwishHelper::DECLINED_RESULT ||
                $response === SwishHelper::CANCELLED_RESULT
            ) {
                $this->swishHelper->deletePaymentFromPendingList($quote->getId());

                return $resultJson->setData(['status' => 'cancelled']);
            }

            if ($response) {
                return $resultJson->setData(!$this->swishHelper->isPaymentReferenceExists($response) ? [
                    'status' => 'paid',
                    'paymentReference' => $response
                ] : [
                    'status' => 'error',
                    'message' => 'The order with that payment reference already exists!'
                ]);
            }

            return $resultJson->setData([
                'status' => 'processing',
                'url' => $url,
                'delay_time' => $this->swishHelper->getConfig(SwishHelper::REQUEST_DELAY) * 1000
            ]);
        } catch (\RuntimeException $exception) {
            $this->swishLogger->addError(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Payment failed',
                    $exception->getMessage()
                )
            );

            return $resultJson->setData([
                'status' => 'error',
                'message' => $exception->getMessage()
            ]);
        }
    }
}
