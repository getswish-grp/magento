<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Controller\Request;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Framework as Framework;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger as SwishLogger;
use Swish\Payment\Model\SwishApi;

class Payment extends Framework\App\Action\Action
{
    /**
     * @var Cart
     */
    public $cart;

    /**
     * @var SwishLogger
     */
    public $swishLogger;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var Framework\Controller\ResultFactory
     */
    public $resultFactory;

    /**
     * Payment constructor
     *
     * @param Framework\App\Action\Context $context
     * @param Cart $cart
     * @param SwishApi $swishApi
     * @param SwishHelper $swishHelper
     * @param SwishLogger $swishLogger
     * @param Framework\Controller\ResultFactory $resultFactory
     */
    public function __construct(
        Framework\App\Action\Context $context,
        Cart $cart,
        SwishApi $swishApi,
        SwishHelper $swishHelper,
        SwishLogger $swishLogger,
        Framework\Controller\ResultFactory $resultFactory
    ) {
        parent::__construct($context);
        $this->cart = $cart;
        $this->swishLogger = $swishLogger;
        $this->swishHelper = $swishHelper;
        $this->swishApi = $swishApi;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute payment request
     *
     * @return Framework\App\ResponseInterface|Framework\Controller\ResultInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $isQRCodeRequest = $this->getRequest()->getParam('isQRRequest');
        $quote = $this->cart->getQuote();

        $data = $this->swishHelper->getDataForPayment(
            $quote,
            $isQRCodeRequest,
            $this->getRequest()->getParam('phone')
        );
        $resultJson = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);

        try {
            $response = $this->swishApi->sendRequest(
                $this->swishHelper->getConfig(SwishHelper::XML_PATH_REQUEST_PAYMENTS_URL, true),
                SwishHelper::POST,
                json_encode($data)
            );

            $this->swishHelper->logPayment(
                $response['Location'],
                $quote->getId(),
                $this->getRequest()->getParam('email')
            );

            $this->swishLogger->addInfo(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Payment request is created.'
                )
            );

            if ($isQRCodeRequest) {
                return $resultJson->setData([
                    'svg' => $this->swishApi->getQRCode(
                        $response['Paymentrequesttoken'] ?? $response['PaymentRequestToken']
                    ),
                    'url' => $response['Location']
                ]);
            }

            $paymentReference = $this->swishApi->retrieveResult($response['Location']);

            return $resultJson->setData(
                $paymentReference ? [
                    'status' => 'paid',
                    'paymentReference' => $paymentReference
                ] : [
                    'status' => 'processing',
                    'url' => $response['Location'],
                    'delay_time' => $this->swishHelper->getConfig(SwishHelper::REQUEST_DELAY) * 1000
                ]
            );
        } catch (\RuntimeException $exception) {
            $this->swishLogger->addError(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Payment failed',
                    $exception->getMessage()
                )
            );

            return $resultJson->setData([
                'status' => 'error',
                'message' => $exception->getMessage()
            ]);
        }
    }
}
