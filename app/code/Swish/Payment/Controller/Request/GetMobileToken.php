<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Controller\Request;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Framework as Framework;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Logger\Logger as SwishLogger;
use Swish\Payment\Model\SwishApi;

class GetMobileToken extends Framework\App\Action\Action
{
    /**
     * @var Cart
     */
    public $cart;

    /**
     * @var SwishLogger
     */
    public $swishLogger;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var SwishApi
     */
    public $swishApi;

    /**
     * @var Framework\Controller\ResultFactory
     */
    public $resultFactory;

    /**
     * @var Framework\Url\EncoderInterface
     */
    public $encoder;

    /**
     * Payment constructor
     *
     * @param Framework\App\Action\Context $context
     * @param Cart $cart
     * @param SwishApi $swishApi
     * @param SwishHelper $swishHelper
     * @param SwishLogger $swishLogger
     * @param Framework\Controller\ResultFactory $resultFactory
     * @param Framework\Url\EncoderInterface $encoder
     */
    public function __construct(
        Framework\App\Action\Context $context,
        Cart $cart,
        SwishApi $swishApi,
        SwishHelper $swishHelper,
        SwishLogger $swishLogger,
        Framework\Controller\ResultFactory $resultFactory,
        Framework\Url\EncoderInterface $encoder
    ) {
        parent::__construct($context);
        $this->cart = $cart;
        $this->swishLogger = $swishLogger;
        $this->swishHelper = $swishHelper;
        $this->swishApi = $swishApi;
        $this->resultFactory = $resultFactory;
        $this->encoder = $encoder;
    }

    /**
     * Execute mobile token request
     *
     * @return Framework\App\ResponseInterface|Framework\Controller\ResultInterface
     * @throws Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);
        $quote = $this->cart->getQuote();
        $data = $this->swishHelper->getDataForPayment($quote, true);
        $email = $this->getRequest()->getParam('email');

        try {
            $response = $this->swishApi->sendRequest(
                $this->swishHelper->getConfig(SwishHelper::XML_PATH_REQUEST_PAYMENTS_URL, true),
                SwishHelper::POST,
                json_encode($data)
            );

            $this->swishLogger->addInfo(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Payment request is created'
                )
            );

            return $resultJson->setData([
                'token' => $response['Paymentrequesttoken'] ?? $response['PaymentRequestToken'],
                'callbackUrl' => urlencode(
                    sprintf(
                        '%s?resultUrl=%s&quoteId=%s&email=%s',
                        $this->swishHelper->getCallbackUrl(),
                        $response['Location'],
                        (string) $quote->getId(),
                        $email
                    )
                )
            ]);
        } catch (\RuntimeException $exception) {
            $this->swishLogger->addError(
                $this->swishHelper->getDataForLogger(
                    $quote,
                    'Mobile Token Request Failed',
                    $exception->getMessage()
                )
            );

            return $resultJson->setData([
                'status' => 'error',
                'message' => $exception->getMessage()
            ]);
        }
    }
}
