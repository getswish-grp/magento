<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Controller\Checkout;

use Magento\Checkout\Model\Cart;
use Magento\Framework as Framework;
use Swish\Payment\Helper\SwishHelper;

class CheckOpenTransaction extends Framework\App\Action\Action
{
    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var Framework\Controller\ResultFactory
     */
    public $resultFactory;

    /**
     * @var Cart
     */
    public $cart;

    /**
     * Payment constructor
     *
     * @param Framework\App\Action\Context $context
     * @param SwishHelper $swishHelper
     * @param Framework\Controller\ResultFactory $resultFactory
     * @param Cart $cart
     */
    public function __construct(
        Framework\App\Action\Context $context,
        SwishHelper $swishHelper,
        Framework\Controller\ResultFactory $resultFactory,
        Cart $cart
    ) {
        parent::__construct($context);
        $this->swishHelper = $swishHelper;
        $this->resultFactory = $resultFactory;
        $this->cart = $cart;
    }

    /**
     * Execute payment request
     *
     * @return Framework\App\ResponseInterface|Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);

        return $resultJson->setData(
            $this->swishHelper->getPaymentStatus($this->cart->getQuote()->getId())
        );
    }
}
