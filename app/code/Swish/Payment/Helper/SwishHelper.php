<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Helper;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Store\Model\StoreManagerInterface;
use Swish\Payment\Model\PaymentRecordFactory;
use Swish\Payment\Model\PaymentReferenceFactory;

class SwishHelper
{
    const SCOPE_TYPE_WESITE = 'websites';
    const XML_PATH_REQUEST_PAYMENTS_URL = 'payment/swish/request_payments_url';
    const XML_PATH_ENVIROMENT = 'payment/swish/environment';
    const XML_REFUNDS_URL = 'payment/swish/refunds_url';
    const XML_QR_CODE_URL = 'payment/swish/qr_code_url';
    const XML_CERT_PASS = 'payment/swish/cert_pass';
    const XML_CERT_FILE = 'payment/swish/cert_file';
    const XML_CERT_KEY_FILE = 'payment/swish/cert_key_file';
    const XML_LOGGER_CONFIG = 'payment/swish/logger';
    const XML_SWISH_NUMBER = 'payment/swish/swish_number';
    const REQUEST_DELAY = 'payment/swish/retrieve_request_delay';
    const SHOULD_CREATE_INVOICE = 'payment/swish/create_invoice';
    const SHOULD_SEND_INVOICE = 'payment/swish/send_invoice';

    const SUCCESS_RESULT = 'PAID';
    const DECLINED_RESULT = 'DECLINED';
    const ERROR_RESULT = 'ERROR';
    const CANCELLED_RESULT = 'CANCELLED';
    const CANCELLED_BY_BANKID_RESULT = 'BANKIDCL';

    const GET = 'GET';
    const PATCH = 'PATCH';
    const POST = 'POST';

    /**
     * @var ScopeConfigInterface
     */
    public $scopeConfigInterface;

    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var EncryptorInterface
     */
    public $enc;

    /**
     * @var UrlInterface
     */
    public $url;

    /**
     * @var PaymentRecordFactory
     */
    public $paymentRecordFactory;

    /**
     * @var PaymentReferenceFactory
     */
    public $paymentReferenceFactory;

    /**
     * Swish helper constructor
     *
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param StoreManagerInterface $storeManager
     * @param EncryptorInterface $enc
     * @param UrlInterface $url
     * @param PaymentRecordFactory $paymentRecordFactory
     * @param PaymentReferenceFactory $paymentReferenceFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        StoreManagerInterface $storeManager,
        EncryptorInterface $enc,
        UrlInterface $url,
        PaymentRecordFactory $paymentRecordFactory,
        PaymentReferenceFactory $paymentReferenceFactory
    ) {
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->storeManager = $storeManager;
        $this->enc = $enc;
        $this->url = $url;
        $this->paymentRecordFactory = $paymentRecordFactory;
        $this->paymentReferenceFactory = $paymentReferenceFactory;
    }

    /**
     * Get Config
     *
     * @param string $path
     * @param bool $isUrl
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getConfig($path, $isUrl = false)
    {
        return $this->scopeConfigInterface->getValue(
            $isUrl ? $path . '_'. $this->getEnvironment() : $path,
            self::SCOPE_TYPE_WESITE,
            $this->storeManager->getStore()->getWebsiteId()
        );
    }

    /**
     * Get Swish Environment
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getEnvironment()
    {
        return $this->scopeConfigInterface->getValue(
            self::XML_PATH_ENVIROMENT,
            self::SCOPE_TYPE_WESITE,
            $this->storeManager->getStore()->getWebsiteId()
        );
    }

    /**
     * Get Certificate password
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getCertPass() : string
    {
        return $this->enc->decrypt(
            $this->scopeConfigInterface->getValue(
                self::XML_CERT_PASS,
                self::SCOPE_TYPE_WESITE,
                $this->storeManager->getStore()->getWebsiteId()
            )
        );
    }

    /**
     * Get Callback Url
     *
     * @return string
     */
    public function getCallbackUrl() : string
    {
        return $this->url->getUrl('swish/result/get', ['_secure' => true]);
    }

    /**
     * Log Payment Record to the DB
     *
     * @param string $url
     * @param $quoteId
     * @param string $email
     * @return bool
     * @throws Exception
     */
    public function logPayment($url, $quoteId, $email) : bool
    {
        $paymentRecord = $this->paymentRecordFactory->create();

        $paymentRecord->load($quoteId, 'quote_id');

        if (!$paymentRecord->getId()) {
            $paymentRecord->setData([
                'location' => $url,
                'quote_id' => $quoteId,
                'is_completed' => false,
                'email' => $email
            ])->save();
        }

        return true;
    }

    /**
     * Mark payment as completed (mean that order already placed)
     *
     * @param $quoteId
     * @return bool
     */
    public function markSwishPaymentAsCompleted($quoteId) : bool
    {
        $paymentRecord = $this->paymentRecordFactory->create();

        $paymentRecord->load($quoteId, 'quote_id')
            ->setIsCompleted(true)
            ->save();

        return true;
    }

    /**
     * Save payment reference to protect payment from double use
     *
     * @param string $reference
     * @param $incrementId
     * @return bool
     * @throws Exception
     */
    public function savePaymentReference($reference, $incrementId) : bool
    {
        $paymentReference = $this->paymentReferenceFactory->create();
        $paymentReference->setData([
            'payment_reference' => $reference,
            'incrementId' => $incrementId
        ])->save();

        return true;
    }

    /**
     * Check is order with this payment reference already placed
     *
     * @param $reference
     * @return bool
     */
    public function isPaymentReferenceExists($reference) : bool
    {
        $paymentReference = $this->paymentReferenceFactory->create();

        $paymentReference->load($reference, 'payment_reference');

        return (bool) $paymentReference->getId();
    }

    /**
     * Remove payment record from swish payment pending table when it's cancelled
     *
     * @param $quoteId
     * @return bool
     * @throws Exception
     */
    public function deletePaymentFromPendingList($quoteId) : bool
    {
        $paymentRecord = $this->paymentRecordFactory->create();

        $paymentRecord->load($quoteId, 'quote_id')
            ->delete();

        return true;
    }

    /**
     * Get Data Array for request
     *
     * @param $quote
     * @param $isQROrMobileRequest
     * @param string $phoneNumber
     * @return array
     * @throws NoSuchEntityException
     */
    public function getDataForPayment($quote, $isQROrMobileRequest, $phoneNumber = '') : array
    {
        $data = [
            'callbackUrl' => $this->getCallbackUrl(),
            'payeeAlias' => $this->getConfig(self::XML_SWISH_NUMBER),
            'amount' => (float) $quote->getGrandTotal(),
            'currency' => $quote->getBaseCurrencyCode(),
        ];

        if (!$isQROrMobileRequest) {
            $data['payerAlias'] = str_replace('+', '', $phoneNumber);
        }

        return $data;
    }

    /**
     * Get data for swish logger
     *
     * @param $quote
     * @param string $status
     * @param string $error
     * @param string $transactionId
     * @param string $orderId
     * @return false|string
     * @throws NoSuchEntityException
     */
    public function getDataForLogger($quote, $status, $error = '', $transactionId = '', $orderId = '')
    {
        $logData = [
            'status' => $status,
            'message' => $error ?? '-',
            'orderId' => $orderId ?? '-',
            'transactionId' => $transactionId ?? '-'
        ];

        switch ($this->getConfig(self::XML_LOGGER_CONFIG)) {
            case 'full':
                $logData['merchantId'] = $this->getConfig(self::XML_SWISH_NUMBER);
                $logData['telephoneNumber'] = $quote->getBillingAddress()->getTelephone();

                return json_encode($logData);
            case 'part':
                return json_encode($logData);
            case 'disabled':
            default:
                return __('Swish logger is disabled. Enable swish logger to handle error/information');
        }
    }

    /**
     * Get payment status
     *
     * @param $quoteId
     * @return bool
     */
    public function getPaymentStatus($quoteId) : bool
    {
        $paymentRecord = $this->paymentRecordFactory->create();

        $paymentRecord->load($quoteId, 'quote_id');

        if ($paymentRecord->getId()) {
            return !$paymentRecord->getIsCompleted();
        }

        return false;
    }
}
