<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Test\Unit\Model\Payment;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Swish\Payment\Model\Payment\Swish;
use PHPUnit\Framework\TestCase;

class SwishTest extends TestCase
{
    /**
     * @var object
     */
    public $paymentSwish;

    /**
     * @var bool
     */
    public $expectedResult;

    /**
     * Run before test
     */
    public function setUp() : void
    {
        $objectManager = new ObjectManager($this);
        $this->paymentSwish = $objectManager->getObject(Swish::class);
        $this->expectedResult = Swish::CONFIG_PAYMENT_ACTION;
    }

    /**
     * Test function
     */
    public function testGetConfigPaymentAction() : void
    {
        $this->assertEquals($this->expectedResult, $this->paymentSwish->getConfigPaymentAction());
    }
}
