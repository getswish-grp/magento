#Swish_Payment changelog
1.0.4: - Added table `swish_payment_references` to check all used references

1.0.3: - Added `email` column to the pending payment table to handle virtual product creation throw cron

1.0.2: - Added table `swish_payment_pending` for pending payment (for cron using)

1.0.1: - Created environment url's
