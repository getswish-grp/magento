<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
use Zend_Db_Exception;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrade script
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) : void
    {
        $installer = $setup;

        $installer->startSetup();
        $tableName = $setup->getTable('swish_payment_pending');

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $connection = $setup->getConnection();

            if (!$connection->isTableExists($tableName)) {
                $table = $connection->newTable($tableName)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        ['identity' => true,'unsigned' => false,'nullable' => false,'primary' => true]
                    )
                    ->addColumn(
                        'location',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false]
                    )
                    ->addColumn(
                        'quote_id',
                        Table::TYPE_TEXT,
                        255,
                        ['nullbale' => false]
                    )
                    ->addColumn(
                        'is_completed',
                        Table::TYPE_SMALLINT,
                        null,
                        ['nullbale' => false]
                    )
                    ->setOption('charset', 'utf8');

                $connection->createTable($table);
            }
        }

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            if ($setup->getConnection()->isTableExists($tableName) === true) {
                $connection = $setup->getConnection();

                $connection->addColumn(
                    $tableName,
                    'email',
                    [
                        'type' => Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'Customer email'
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $tableName = $setup->getTable('swish_payment_references');
            $connection = $setup->getConnection();

            if (!$connection->isTableExists($tableName)) {
                $table = $connection->newTable($tableName)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        ['identity' => true,'unsigned' => false,'nullable' => false,'primary' => true]
                    )
                    ->addColumn(
                        'payment_reference',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false]
                    )
                    ->setOption('charset', 'utf8');

                $connection->createTable($table);
            }
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $tableName = $setup->getTable('swish_payment_references');

            if ($setup->getConnection()->isTableExists($tableName) === true) {
                $connection = $setup->getConnection();

                $connection->addColumn(
                    $tableName,
                    'incrementId',
                    [
                        'type' => Table::TYPE_TEXT,
                        'length' => 32,
                        'nullable' => false,
                        'comment' => 'Increment Id'
                    ]
                );
            }
        }

        $installer->endSetup();
    }
}
