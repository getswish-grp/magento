<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var WriterInterface
     */
    public $configInterface;

    /**
     * UpgradeData constructor.
     * @param WriterInterface $configInterface
     */
    public function __construct(
        WriterInterface $configInterface
    ) {
        $this->configInterface = $configInterface;
    }

    /**
     * Upgrade script
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) : void
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $data = [
                'mss' => [
                    'request_payments_url' => 'https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests',
                    'refunds_url' => 'https://mss.cpc.getswish.net/swish-cpcapi/api/v1/refunds',
                    'qr_code_url' => 'https://mpc.getswish.net/qrg-swish/api/v1/commerce',
                    'payout_url' => 'https://mss.cpc.getswish.net/swish-cpcapi/api/v1/payouts'
                ],
                'sandbox' => [
                    'request_payments_url' => 'https://staging.getswish.pub.tds.tieto.com/cpc-swish/api/v1/paymentrequests',
                    'refunds_url' => 'https://staging.getswish.pub.tds.tieto.com/cpc-swish/api/v1/refunds',
                    'qr_code_url' => 'https://staging.getswish.pub.tds.tieto.com/qrg-swish/api/v1/commerce',
                    'payout_url' => 'https://staging.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts'
                ],
                'live' => [
                    'request_payments_url' => 'https://cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests',
                    'refunds_url' => 'https://cpc.getswish.net/swish-cpcapi/api/v1/refunds',
                    'qr_code_url' => 'https://mpc.getswish.net/qrg-swish/api/v1/commerce',
                    'payout_url' => 'https://cpc.getswish.net/swish-cpcapi/api/v1/payouts'
                ]
            ];

            foreach ($data as $environmentKey => $environmentData) {
                foreach ($environmentData as $configPath => $configValue) {
                    $this->configInterface->save(
                        $this->getConfigPath($environmentKey, $configPath),
                        $configValue
                    );
                }
            }
        }
    }

    /**
     * Get swish payment config path
     *
     * @param $environment
     * @param $path
     * @return mixed
     */
    public function getConfigPath($environment, $path)
    {
        return sprintf(
            'payment/swish/%s_%s',
            $path,
            $environment
        );
    }
}
