<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Logger implements ArrayInterface
{
    /**
     * Return array of options as value-label pairs, eg. value => label
     *
     * @return array
     */
    public function toOptionArray() : array
    {
        return [
            ['value' => 'disabled', 'label' => __('Disabled')],
            ['value' => 'full', 'label' => __('Enabled with sensitive data')],
            ['value' => 'part', 'label' => __('Enabled without sensitive data')]
        ];
    }
}
