<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Model\Config\Backend;

use Magento\Config\Model\Config\Backend\File;

class CertKey extends File
{
    /**
     * Get Allowed extensions for uploaded certificateg
     *
     * @return array
     */
    public function getAllowedExtensions()
    {
        return ['key'];
    }
}
