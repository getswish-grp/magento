<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Model\Payment;

use Magento\Payment\Model as PaymentModel;

class Swish extends PaymentModel\Method\AbstractMethod
{
    const CODE = 'swish';
    const CONFIG_PAYMENT_ACTION = 'authorize';
    protected $_code = self::CODE;

    /**
     * Payment Method feature
     *
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * Payment refund
     *
     * @param PaymentModel\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function refund(PaymentModel\InfoInterface $payment, $amount) : self
    {
        // Refund Request method

        return $this;
    }

    /**
     * Payment action getter compatible with payment model
     *
     * @return string
     */
    public function getConfigPaymentAction() : string
    {
        return self::CONFIG_PAYMENT_ACTION;
    }
}
