<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Model\ResourceModel\References;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Swish\Payment\Model\PaymentReference;
use Swish\Payment\Model\ResourceModel\PaymentReferenceResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'swish_payment_references_collection';
    protected $_eventObject = 'swish_payment_references_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() : void
    {
        $this->_init(PaymentReference::class, PaymentReferenceResource::class);
    }
}
