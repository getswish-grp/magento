<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Model;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Filesystem\DirectoryList as DirectoryList;
use Magento\Sales\Model\Order;
use RuntimeException;
use Swish\Payment\Helper\SwishHelper;
use Swish\Payment\Model\Payment\Swish;

class SwishApi extends Curl
{
    /**
     * @var DirectoryList
     */
    public $directoryList;

    /**
     * @var SwishHelper
     */
    public $swishHelper;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * SwishApi constructor
     *
     * @param DirectoryList $directoryList
     * @param SwishHelper $swishHelper
     * @param Cart $cart
     * @param null $sslVersion
     */
    public function __construct(
        DirectoryList $directoryList,
        SwishHelper $swishHelper,
        Cart $cart,
        $sslVersion = null
    ) {
        $this->directoryList = $directoryList;
        $this->swishHelper = $swishHelper;
        parent::__construct($sslVersion);
        $this->cart = $cart;
    }

    /**
     * Send request method
     *
     * @param string $url
     * @param string $type
     * @param string $data
     * @param bool $isPaymentRequest
     * @return array|bool|mixed|string
     * @throws FileSystemException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function sendRequest($url, $type, $data = '', $isPaymentRequest = true)
    {
        if ($type !== SwishHelper::PATCH) {
            $this->addHeader('Content-Type', 'application/json');
        }

        if ($isPaymentRequest) {
            $this->assignCertificate();
        }

        switch ($type) {
            case SwishHelper::GET:
                $this->get($url);
                $body = $this->getBody();

                return $this->processResponse($body);
            case SwishHelper::POST:
                $this->post($url, $data);
                $body = (array) json_decode($this->getBody());
                $this->checkForErrors(isset($body['0']) ? (array) $body['0'] : $body);

                return $isPaymentRequest ? $this->getHeaders() : $this->getBody();
            case SwishHelper::PATCH:
                $this->addHeader('Content-Type', 'application/json-patch+json');
                $this->setOption(CURLOPT_POSTFIELDS, $data);
                $this->makeRequest(SwishHelper::PATCH, $url);
                $body = $this->getBody();

                return $this->processResponse($body);
            default:
                throw new RuntimeException(__('incorrect request type'));
        }
    }

    /**
     * Handle response error
     *
     * @param $body
     * @throws Exception
     */
    public function checkForErrors($body) : void
    {
        if (isset($body['errorCode'])) {
            $this->checkErrorCode($body['errorCode']);
            throw new RuntimeException($body['errorMessage']);
        }

        if (isset($body[0]->errorCode)) {
            $this->checkErrorCode($body[0]->errorCode);
            throw new RuntimeException($body[0]->errorMessage);
        }

        if (isset($body['error'])) {
            $this->checkErrorCode($body['error']);
            throw new RuntimeException($body['message']);
        }
    }

    /**
     * Check error code for additional action
     *
     * @param $errorCode
     * @throws Exception
     */
    public function checkErrorCode($errorCode)
    {
        if ($errorCode === SwishHelper::CANCELLED_BY_BANKID_RESULT) {
            $this->swishHelper->deletePaymentFromPendingList($this->cart->getQuote()->getId());
        }
    }

    /**
     * Process Response
     *
     * @param $body
     * @return bool|mixed
     * @throws Exception
     */
    public function processResponse($body)
    {
        $data = (array) json_decode($body);

        $this->checkForErrors($data);

        switch ($data['status']) {
            case SwishHelper::ERROR_RESULT:
                throw new RuntimeException(__('Error: ' . $data['errorMessage']));
            case SwishHelper::SUCCESS_RESULT:
                return $data['paymentReference'];
            case SwishHelper::DECLINED_RESULT:
            case SwishHelper::CANCELLED_RESULT:
                return $data['status'];
            default:
                return false;
        }
    }

    /**
     * Assign Certificate
     *
     * @throws FileSystemException
     * @throws NoSuchEntityException
     */
    public function assignCertificate() : void
    {
        $certFilename = $this->swishHelper->getConfig(SwishHelper::XML_CERT_FILE);
        $pubDirPath = $this->directoryList->getPath('pub');
        $certExtention = pathinfo($certFilename, PATHINFO_EXTENSION);

        $this->setOption(CURLOPT_SSLCERT, sprintf('%s/cert/%s', $pubDirPath, $certFilename));
        $this->setOption(CURLOPT_SSLCERTTYPE, $certExtention);
        $this->setOption(CURLOPT_SSLCERTPASSWD, $this->swishHelper->getCertPass());

        if (strtolower($certExtention) === 'pem') {
            $this->setOption(CURLOPT_SSLKEY, sprintf(
                '%s/cert/%s',
                $pubDirPath,
                $this->swishHelper->getConfig(SwishHelper::XML_CERT_KEY_FILE)
            ));
        }
    }

    /**
     * Get QR code
     *
     * @param string $token
     * @return false|string
     * @throws FileSystemException
     * @throws NoSuchEntityException
     */
    public function getQRCode($token)
    {
        $data = [
            'format' => 'svg',
            'size' => 300,
            'token' => $token
        ];
        $result = $this->sendRequest(
            $this->swishHelper->getConfig(SwishHelper::XML_QR_CODE_URL, true),
            SwishHelper::POST,
            json_encode($data),
            false
        );

        return substr($result, strpos($result, '<svg'));
    }

    /**
     * Create refund request
     *
     * @param Order $order
     * @return array|bool|mixed|string
     * @throws FileSystemException
     * @throws NoSuchEntityException
     */
    public function refund($order)
    {
        if (isset($order->getPayment()->getAdditionalInformation()['payment_reference'])) {
            $paymentReference = $order->getPayment()->getAdditionalInformation()['payment_reference'] ;
        } else {
            throw new RuntimeException(__('Order do not have original PaymentReference'));
        }

        $data = [
            'originalPaymentReference' => $paymentReference,
            'callbackUrl' => $this->swishHelper->getCallbackUrl(),
            'payerAlias' => $this->swishHelper->getConfig(SwishHelper::XML_SWISH_NUMBER),
            'payeeAlias' => $order->getBillingAddress()->getTelephone(),
            'amount' => (float) $order->getGrandTotal(),
            'currency' => $order->getBaseCurrencyCode()
        ];

        $body = $this->sendRequest(
            $this->swishHelper->getConfig(SwishHelper::XML_REFUNDS_URL, true),
            SwishHelper::POST,
            json_encode($data)
        );

        $this->checkForErrors($body);

        return $body;
    }

    /**
     * Retrieve Result
     *
     * @param string $url
     * @return bool|string
     * @throws FileSystemException
     * @throws NoSuchEntityException
     */
    public function retrieveResult($url)
    {
        return $this->sendRequest($url, SwishHelper::GET);
    }

    /**
     * Create cancel payment request
     *
     * @param string $url
     * @return array|bool|string
     * @throws FileSystemException
     * @throws NoSuchEntityException
     */
    public function cancelPayment($url)
    {
        if ($this->retrieveResult($url)) {
            throw new RuntimeException(__('Error: Order already is cancelled/paid and could not be cancelled'));
        }

        return $this->sendRequest(
            $url,
            SwishHelper::PATCH,
            json_encode([['op' => 'replace', 'path' => '/status', 'value' => 'cancelled']])
        );
    }
}
