<?php
/**
 * @category  Swish
 * @package   Swish_Payment
 * @author    Ivans Zuks <info@scandiweb.com>
 * @copyright Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license   https://opensource.org/licenses/AFL-3.0 The Academic Free License 3.0 (AFL-3.0)
 */
declare(strict_types=1);

namespace Swish\Payment\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Swish\Payment\Model\ResourceModel\PaymentReferenceResource;

class PaymentReference extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'swish_payment_references';

    protected $_cacheTag = 'swish_payment_references';

    protected $_eventPrefix = 'swish_payment_references';

    /**
     *  Model Construct
     */
    protected function _construct() : void
    {
        $this->_init(PaymentReferenceResource::class);
    }

    /**
     * Get Identities
     *
     * @return string[]
     */
    public function getIdentities() : array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Default Values
     *
     * @return array
     */
    public function getDefaultValues() : array
    {
        return [];
    }
}
